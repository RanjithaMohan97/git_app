import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/User-account/App.js';
import './App.css';
import {reducer} from './Reducers/reducer.js';
import { Provider } from 'react-redux';
import { createStore} from 'redux';
import {details} from'./InitialState.js';


const store = createStore(reducer,details);

ReactDOM.render(
<Provider store = {store}>
    <App/>
</Provider>, document.getElementById('root'));



