import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Link } from 'react-router-dom';
import {connect} from 'react-redux';
import {updateUserName} from '../Actions/LoginAction.js';

const mapStateToProps = (state) => {
  console.log(state);
  return {
    stateInfo:state
  }
}
const mapDispatchToProps = (dispatch) =>{
  return{
    setUser:(name)=>dispatch(updateUserName(name))
  }
}
class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      userName : "",
      password : "",
      getMenu : false
    }
    this.handleChange = this.handleChange.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
  }
  handleChange(event){
    console.log(this.props.stateInfo.userName);
    this.setState({userName : event.target.value})
  

  }
  handlePassword(event){
    console.log(this.state.password);
    this.setState({password : event.target.value})
  }
  handleLogin() {
    this.props.setUser(this.state.userName);
    this.setState({getMenu : true})

   
    
  }

  render() {
    console.log(this.state.getMenu)
    return (
      <div className="App">
        <TextField
          label="Name"
          value={this.state.userName}
          onChange={e => this.handleChange(e)}
          margin="normal"
          variant="outlined"
        /><br/>
        <TextField
          label="Password"
          value={this.state.password}
          onChange={e => this.handlePassword(e)}
          margin="normal"
          variant="outlined"
        /><br/>
        <Link to="/menuBar" style ={{textDecoration:"none"}}>
          <Button variant="contained" onClick={ () => this.handleLogin() }>
                login
          </Button>
          </Link>
         
      </div>
    );
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);
