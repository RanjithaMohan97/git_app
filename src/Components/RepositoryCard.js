import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Star from '@material-ui/icons/Grade';
import Fork from '@material-ui/icons/CallSplit';
import LanguageIcon from '@material-ui/icons/Lens';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    console.log(state);
    return {
      stateInfo:state
    }
  }
  const mapDispatchToProps = (dispatch) =>{
    return{

    }
  }

class RepoDetails extends Component {

    render(){
        return(
            <Card id = "card">
                
                    <CardActions>
                        <Avatar  alt="User profile" src="https://avatars1.githubusercontent.com/u/39079298?s=400&u=c3408a43b5ce005afdbc183a3bec7ac0eb424d92&v=4" />
                            <Typography gutterBottom variant="h5" component="h2">
                                RepoName
                            </Typography>
                        </CardActions>
                    <CardActions>
                        <LanguageIcon id = "lang"/>
                        <Typography component="p">
                            Language
                        </Typography>
                        <Star/>
                        <Fork/>


                      
                </CardActions>
            </Card>
        );
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(RepoDetails);