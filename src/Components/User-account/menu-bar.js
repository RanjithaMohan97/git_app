import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Drawer from '@material-ui/core/Drawer';
import { Button } from '../../../node_modules/@material-ui/core';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { Link,withRouter } from 'react-router-dom';


const mapStateToProps=(state)=>{
  return {userName:state.userName}
}
  const mapDispatchToProps=(dispatch)=>{
  return {};
}

const styles = {
    root: {
      flexGrow: 1,
    },
    grow: {
      flexGrow: 1,
    },
    menuButton: {
      marginLeft: -12,
      marginRight: 20,
    },
    list: {
        width: 250,
      }
  };

class MenuBar extends Component {
    constructor(props)
    {
        super(props);
        this.state ={
            menuOpen:false,
            pageShown:"none"
        }
    }

    toggleMenu = () => {
        this.setState({menuOpen:!this.state.menuOpen})
    }
    render() {
        const { classes } = this.props;
        console.log("I am MenuBar");
        return(
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
                            <MenuIcon onClick={ this.toggleMenu }/>
                        </IconButton>
                        <Typography variant="h6" color="inherit" className={classes.grow}>
                            Menu
                        </Typography>
                        <AccountCircle />
                        <Typography color="inherit">{ this.props.userName }</Typography>
                    </Toolbar>
                </AppBar>
                    <Drawer open={ this.state.menuOpen } anchor="left">
                        <div className={classes.list}>
                            <List>
                                <ListItem>
                                    <ListItemText>
                                    <Link to="/profile" onClick={ ()=> this.toggleMenu() } style ={{textDecoration:"none"}}>
                                        Profile
                                    </Link>
                                    </ListItemText>
                                </ListItem>
                                <ListItem>
                                    <ListItemText>
                                    <Link to="/dashBoard" onClick={ ()=> this.toggleMenu() } style ={{textDecoration:"none"}}>
                                        DashBoard
                                    </Link>
                                    </ListItemText>
                                </ListItem>
                            </List>
                            <Button variant="contained" color="primary" id="menuClose" onClick={ this.toggleMenu }>Close</Button>
                        </div>
                    </Drawer>
            </div>
        );
    }
}

MenuBar.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(MenuBar));
//export default MenuBar;