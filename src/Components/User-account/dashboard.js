import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { fade } from '@material-ui/core/styles/colorManipulator';
//import SearchBar from 'material-ui-search-bar';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import MenuBar from './menu-bar';
import { Menu } from '@material-ui/core';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';


const styles = theme => ({
    textField: {
        
      },
      button: {
       
      },
      menuButton: {
        marginLeft: -12,
        marginRight: 20,
      },
      inputRoot: {
        color: 'inherit',
        width: '100%',
        
      },
      inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
          width: 200,
        },
        border: '1px solid black',
        borderRadius: '5px',
      },
      search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
          backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
          marginLeft: theme.spacing.unit * 3,
          width: 'auto',
        },
      },
      searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      },
      formControl: {
        margin: theme.spacing.unit,
        marginLeft: '20%',
        minWidth: 120,
      },
  });

class DashBoard extends Component {

    constructor(props) {
      super(props);
      this.state={
        LanguageDialogOpen:false,
        SortDialogOpen:false,
        language:"Any",
        sort:"Best Match",
      }
      this.handleChange = this.handleChange.bind(this);
    }

    OpenLanguageDialogue=()=> {
      this.setState({LanguageDialogOpen:true})
    }
    CloseLanguageDialogue=()=> {
      this.setState({LanguageDialogOpen:false})
    }
    OpenSortDialogue=()=> {
      this.setState({SortDialogOpen:true})
    }
    CloseSortDialogue=()=> {
      this.setState({SortDialogOpen:false})
    }


    handleChange = (event) => {
      this.setState({[event.name]: event.value });
    };
   
    setsearchString = (event) => {
      var code = event.keyCode || event.which;
      if(code === 13)
      {
        console.log("Search the string")
      }
    }
    render() {
        const { classes } = this.props;
        return (
          <div>
            <div id="searchBar">
                    <div className={classes.search}>
                    <div className={classes.searchIcon}>
                    <SearchIcon/>
                    </div>
                     <InputBase
                            placeholder="Search…"
                            classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                            }}
                            id="searchField"
                            onKeyPress={ (e)=>this.setsearchString(e) }
                            />
                      </div>
                      <FormControl className={classes.formControl}>
                      <InputLabel htmlFor="language-select">Language</InputLabel>
                      <Select
                      open={this.state.dialogOpen}
                      onClose={this.handleClose}
                      onOpen={this.handleOpen}
                      value={this.state.language}
                      onChange={e=>this.handleChange(e.target)}
                      inputProps={{
                        name: 'language', 
                        id: 'language-select',
                      }}
                      >
                        <MenuItem value={"Any"}>Any</MenuItem>
                        <MenuItem value={"Java"}>Java</MenuItem>
                        <MenuItem value={"JavaScript"}>JavaScript</MenuItem>
                        <MenuItem value={"C++"}>C++</MenuItem>
                        <MenuItem value={"C#"}>C#</MenuItem>
                        <MenuItem value={"Python"}>Python</MenuItem>
                        <MenuItem value={"HTML"}>HTML</MenuItem>
                        <MenuItem value={"C"}>C</MenuItem>
                        <MenuItem value={"Processing"}>Processing</MenuItem>
                        <MenuItem value={"Swift"}>Swift</MenuItem>
                        <MenuItem value={"CSS"}>CSS</MenuItem>
                      </Select>
                      </FormControl>
                      <FormControl className={classes.formControl}>
                      <InputLabel htmlFor="sort-select">Sort</InputLabel>
                      <Select
                      open={this.state.SortDialogOpen}
                      onClose={this.CloseSortDialogue}
                      onOpen={this.OpenSortDialogue}
                      value={this.state.sort}
                      onChange={e=>this.handleChange(e.target)}
                      inputProps={{
                        name: 'sort',
                        id: 'sort-select',
                      }}
                      >
                        <MenuItem value={"Best Match"}>Best Match</MenuItem>
                        <MenuItem value={"Most Stars"}>Most Stars</MenuItem>
                        <MenuItem value={"Fewest Stars"}>Fewest Stars</MenuItem>
                        <MenuItem value={"Most Forks"}>Most Forks</MenuItem>
                        <MenuItem value={"Fewest Forks"}>Fewest Forks</MenuItem>
                        <MenuItem value={"Recently Updated"}>Recently Updated</MenuItem>
                        <MenuItem value={"Least Recently Updated"}>Least Recently Updated</MenuItem>
                      </Select>
                      </FormControl>
                      </div>
          </div>
        );
    }
}

DashBoard.propTypes = {
    classes: PropTypes.object.isRequired,
  };
  
 export default withStyles(styles)(DashBoard);

 //export default DashBoard;