import React, { Component } from 'react';
import Pages from './Pages';
import Login from '../Login';
import { BrowserRouter as Router, Route ,Switch} from 'react-router-dom'

class App extends Component{
render(){
    console.log("I am Inside Router comp")
    return(
        <div>
        <Router>
            <Switch>
                <Route exact path="/" render = {()=>(<Login/>)}/>
                <Pages />
            </Switch>
        </Router>        
    </div>
    );
}
}
export default App;