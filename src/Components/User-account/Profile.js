import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { Link,withRouter } from 'react-router-dom';
import {connect} from 'react-redux';
//import {updateUserName} from '../Actions/LoginAction.js';
import Avatar from '@material-ui/core/Avatar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MenuBar from './menu-bar';


const mapStateToProps = (state) => {
  console.log(state);
  return {
    stateInfo:state
  }
}
const mapDispatchToProps = (dispatch) =>{
  return{
    //setUser:(name)=>dispatch(updateUserName(name))
  }
}
const styles = theme => ({
    root: {
      width:'90%'
    },
    tabspace: {
      flexGrow: 1
    }
  });
class Profile extends Component {

    constructor(props){
        super(props);
        this.state = {
            value:0
        }
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange (event, value){
        this.setState({ value });
      }

    render(){
        const { classes } = this.props;

        return(
            <div>
            <div id = "profile">
                <div id = "profileInfo">
                    <Avatar id="profilePic"  alt="display picture" src="https://avatars1.githubusercontent.com/u/39079298?s=400&u=c3408a43b5ce005afdbc183a3bec7ac0eb424d92&v=4" />
                    <h3>{this.props.stateInfo.userName}</h3>
                    <p>Good soul</p>
                </div>
                <div id = "repoInfo">
                <div className={classes.root}>
                    <Paper square>
                        <Tabs textColor="primary" value={this.state.value} onChange={this.handleChange}>
                            <Tab label="Pinned Projects" className = {classes.tabspace}/>
                            <Tab label="Repositories" className = {classes.tabspace}/>
                            <Tab label="Starred Projects" className = {classes.tabspace}/>
                        </Tabs>
                    </Paper>
                    </div>
                </div>
            </div>
            </div>
        );
    }
}

Profile.propTypes = {
    classes: PropTypes.object.isRequired,
    //theme: PropTypes.object.isRequired,
  };
export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles)(Profile));