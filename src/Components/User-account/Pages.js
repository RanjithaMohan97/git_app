import React, { Component } from 'react';
import Profile from './Profile.js';
import DashBoard from './dashboard';
import MenuBar from './menu-bar';

import { BrowserRouter as Router, Route ,Switch} from 'react-router-dom'

class Pages extends Component{
render(){
    console.log("I am Inside Roter comp")
    return(
        <div>
            <MenuBar />
            <div>
                <Route exact path="/profile" render = {()=>(<Profile/>)}/>
                <Route exact path ="/dashBoard" render = {()=>(<DashBoard/>)}/>
            </div>
        </div>
    );
    }
}
export default Pages;